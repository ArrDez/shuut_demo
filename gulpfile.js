/*jslint node: true */
/*global */

(function () {
	'use strict';
	
	var gulp 		= require('gulp'),
		inject 		= require('gulp-inject');

	var paths = {
			libraries: {
				scripts: [
					'node_modules/framework7/dist/js/framework7.js',
					'node_modules/framework7/dist/js/framework7.js.map',
					'node_modules/jquery/dist/jquery.min.js'
				],
				styles: [
					'node_modules/framework7/dist/css/framework7.ios.css',
					'node_modules/framework7/dist/css/framework7.ios.colors.css',
					'node_modules/framework7/dist/css/framework7.material.css',
					'node_modules/framework7/dist/css/framework7.material.colors.css'
				]
			},
			source: {
				root: 'src',
				templates: 'src/templates/**/*.html',
				scripts: 'src/**/*.js',
				styles: 'src/**/*.css',
				views: 'src/views/**/*.js',
				json: 'src/**/*.json'
			},
			dist: {
				root: 'www',
				libraries: 'www/lib',
			}
		};
		
		
	gulp.task('default', [ 'dist', 'watch' ]);

	gulp.task('watch', function () {
		gulp.watch(paths.libraries.scripts.concat(paths.libraries.styles), [ 'libraries' ]);
		gulp.watch('src/**/*.html', [ 'templates' ]);
		gulp.watch(paths.source.scripts, [ 'scripts' ]);
		gulp.watch(paths.source.styles, [ 'styles' ]);
	});

	gulp.task('dist', [ 'libraries', 'templates', 'scripts', 'styles', 'json']);
	
	gulp.task('libraries', function () {
		return gulp.src(paths.libraries.scripts.concat(paths.libraries.styles))
			.pipe(gulp.dest(paths.dist.libraries));
	});


	gulp.task('templates', function () {
		gulp.src(  'src/index.html')
  			.pipe(inject( gulp.src( paths.source.views, {read: false}), {
  				relative: true,
  				starttag: '<!-- inject:views -->',
  			}))
		  	.pipe(inject( gulp.src([ paths.source.templates ] ), {
		  		starttag: '<!-- inject:templates -->',
				transform: function (filepath, file) {
					if ( file.path.indexOf('/') != -1  ){
						var arr = file.path.split('/');
						var name  = arr[ arr.length -1 ].split('.').slice(0, -1).join('.')
					} else {
						var arr = file.path.split('\\');
						var name  = arr[ arr.length -1 ].split('.').slice(0, -1).join('.')
					}
					return '<script id="'+name+'" type="text/template7">' + file.contents.toString('utf8') + '</script>'
				}
		    }
		  ))
		  .pipe(gulp.dest( paths.dist.root ));
	});


	gulp.task('scripts', function () {
		gulp.start('templates');
		return gulp.src(paths.source.scripts)
			.pipe(gulp.dest(paths.dist.root));
	});
	gulp.task('styles', function () {
		return gulp.src(paths.source.styles)
			.pipe(gulp.dest(paths.dist.root));
	});
	gulp.task('json', function () {
		return gulp.src(paths.source.json)
			.pipe(gulp.dest(paths.dist.root));
	});
	
	
}());