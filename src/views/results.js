app.view.results = {
	init: function($el, context, data){
		
		api.request({
			url: 'futusome.json',
			success: function(data) {
				app.storage.set('adverts', data);
				app.mainView.router.load({
					template: Template7.templates.results,
					context: {
						page: {
							name: 'results',
							title: 'Search Results',
							back: true,
						},
						data: data
					},
				});
			}
		});
		
	},
};