app.view.registration = {
	init: function(){
		
		app.mainView.router.load({
			template: Template7.templates.registration,
			context: {
				page: {
					name: 'registration',
					title: 'Sign Up',
					back: true,
				},
				action: '#'
			},
		});
		
	}
};