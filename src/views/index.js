app.view.index = {
	init: function($el, context, data){
		app.mainView.router.load({
			template: Template7.templates.index,
			context: {
				page: {
					name: 'index',
					title: 'Dashboard',
					back: false,
					cssClass: 'toolbar-fixed'
				}
			},
		});
		
	},
};