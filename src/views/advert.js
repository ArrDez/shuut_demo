app.view.advert = {
	init: function($el, context, data){
		var adverts = app.storage.get('adverts');
		console.log(adverts.documents[data.advert_id]);
		app.mainView.router.load({
			template: Template7.templates.advert,
			context: {
				page: {
					name: 'advert',
					title: 'Create Advert',
					back: true
				},
				item: adverts.documents[data.advert_id]
			},
		});
		
	},
};