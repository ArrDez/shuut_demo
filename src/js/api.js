/*global Framework7, Dom7 */
(function (Framework7, $$) {
	'use strict';

	var Api = {
		defaults: {
			method: 'get',
			contentType: false,
			processData: false,
			crossDomain: true,
			dataType: 'json',
			timeout: 5000,
			beforeSend: function() {
				app.F7.showIndicator();
			},
			complete: function(xhr, status) {
				app.F7.hideIndicator();
			},
			statusCode: {
				404: function (xhr) {
					app.notify.error('Please check your internet connection.');
				},
				504: function (xhr) {
					app.notify.error('Please check your internet connection.');
				},
				500: function (xhr) {
					app.notify.error('Please try again later.');
				},
			},
			error: function(xhr, status) {
				app.F7.hideIndicator();
				app.notify.show('<span class="color-red"><i class="fa fa-exclamation-circle" ></i> Error</span>');
			}
		},
		_call: function(params) {
			params = helper.extend(this.defaults, params);
			if(app.User) {
				params.headers = {
					'token': app.User.remember_token
				};
			}
			$$.ajax(params);
		},
		request: function(params) {
			this._call(params);
		}
	};
	
	window.api = Api;
	
}(Framework7, Dom7));