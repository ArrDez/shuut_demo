app.notify = {
	show: function(msg, callback) {
		if(typeof msg === 'string') {
			if(!callback) {
				callback = function() {};
			}
			var el = app.F7.addNotification({
				title: msg,
				onClose: callback
			});
			this.closeOnDelay(el);
			return;
		}
		var el = app.F7.addNotification(msg);
		this.closeOnDelay(el);
		return;
	},
	success: function(msg) {
		msg = '<span class="color-green"><i class="fa fa-circle" ></i> '+msg+'</span>';
		this.show(msg);
	}, 
	error: function(msg) {
		msg = '<span class="color-red"><i class="fa fa-exclamation-circle" ></i> '+msg+'</span>';
		this.show(msg);
	},
	closeOnDelay: function(el) {
		var $el = $(el);
		setTimeout(function() {
			$el.css('opacity', 0);
			setTimeout(function() {
				$el.remove();
			}, 450);
		}, 2000);
	}
};
