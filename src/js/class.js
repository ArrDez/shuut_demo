/*=============================================
=            Observer prototype            =
=============================================*/

Observer = function(){
    this.listeners = [];
};
Observer.prototype.on = function( name , callback ){
    this.listeners.push( {name: name, callback: callback} );
};
Observer.prototype.trigger = function( name , scope ){
    var result = false;
    this.listeners.forEach(function(event) {
        if ( event.name == name ) {
            result = event.callback.call(scope, scope);
        }
    });
    return result;
};
Observer.prototype.remove = function( name ){
	for(var i in this.listeners){
		if (name == this.listeners[i].name ){
			delete this.listeners[i];
		}
	}
	this.listeners = this.listeners.filter(function(n){ return n != undefined }); 
};

/*=============================================
=            LocalStorage service           =
=============================================*/

LocalStorageService = function(){};
LocalStorageService.prototype.set = function( name, value ){
	switch( true ){
		case (typeof value === 'object'):
			localStorage.setItem( name, JSON.stringify( value ) );
		break;

		default: 
			localStorage.setItem( name, value );
		break;
	}
};
LocalStorageService.prototype.get = function( name ){
	try{
	   return JSON.parse( localStorage.getItem( name ) );
	}catch(e){
	   return localStorage.getItem( name );
	}
};
LocalStorageService.prototype.remove = function( name ){
	localStorage.removeItem( name );
};


