app.panel = {
	init: function() {
		var content = Template7.templates.panel();
		$$('.panel').html(content);
	},
	open: function() {
		app.F7.openPanel('right');
	},
	close: function() {
		app.F7.closePanel();
	}
};
$$('.panel').on('click', '.app-link, .app-popup', function() {
	F7.closePanel();
});
