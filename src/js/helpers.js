Template7.registerHelper('ios', function (context, options) {
	if (F7.device.ios) {
		return context.fn(this, context.data);
	}
	return '';
});

Template7.registerHelper('android', function (context, options) {
	if (!F7.device.ios) {
		return context.fn(this, context.data);
	}
	return '';
});

Template7.registerHelper('page', function (context, options) {
	return Template7.templates.page({
		page: context,
		content: options.fn(this, options.data)
	});
});

Template7.registerHelper('navbar', function (context, options) {
	var params = app.mainView.params;
	return Template7.templates.navbar({
		wrapper: !(device.ios && params.dynamicNavbar),
		content: context.fn(this, context.data)
	});
});

/*
 * PORT OF JQUERY EXTEND FUNCTION
 */
var helper = {};

helper.getCallback = function(el) {
	var callback = $$(el).data('callback');
	if(callback) {
		callback = callback.split('::');
		var name = callback[0];
		var func = callback[1];
		if ( app.view[ name ] && app.view[ name ][ func ] ) {
			return app.view[ name ][ func ];
		}
		if ( app.popup[ name ] && app.popup[ name ][ func ] ) {
			return app.popup[ name ][ func ];
		}
	}
	return function(){};
};

helper.extend = function extend() {
	var target = arguments[0] || {};
	var i = 1;
	var length = arguments.length;
	var deep = false;
	var options,
	    name,
	    src,
	    copy,
	    copyIsArray,
	    clone;

	// Handle a deep copy situation
	if ( typeof target === 'boolean') {
		deep = target;
		target = arguments[1] || {};
		// skip the boolean and the target
		i = 2;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== 'object' && !is.fn(target)) {
		target = {};
	}

	for (; i < length; i++) {
		// Only deal with non-null/undefined values
		options = arguments[i];
		if (options != null) {
			if ( typeof options === 'string') {
				options = options.split('');
			}
			// Extend the base object
			for (name in options) {
				src = target[name];
				copy = options[name];

				// Prevent never-ending loop
				if (target === copy) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if (deep && copy && (is.hash(copy) || ( copyIsArray = is.array(copy)))) {
					if (copyIsArray) {
						copyIsArray = false;
						clone = src && is.array(src) ? src : [];
					} else {
						clone = src && is.hash(src) ? src : {};
					}

					// Never move original objects, clone them
					target[name] = extend(deep, clone, copy);

					// Don't bring in undefined values
				} else if ( typeof copy !== 'undefined') {
					target[name] = copy;
				}
			}
		}
	}
	// Return the modified object
	return target;
}; 