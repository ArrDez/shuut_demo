( function(Framework7, $$, T7, moment, api) {
		'use strict';

		var F7 = new Framework7({
			modalTitle : 'Moving Chat',
			template7Pages : true,
			precompileTemplates : true,
			router : true,
			externalLinks : 'a.js-external-link',
			cache : false,
			material : Framework7.prototype.device.android,
			materialRipple : Framework7.prototype.device.android,
			fastClicks : true,
			pushState : false,
			material : !(Framework7.prototype.device.ios)
		});

		var app = {
			F7 : F7,
			view : {},
			popup : {},
			events : new Observer(),
			storage : new LocalStorageService(),
			renderView : function(name, context, data) {
				app.view[name].init($$('#' + name), data, context);
			},
			init : function() {
				if (device.ios && this.mainView.params.dynamicNavbar) {
					this.fixDynNavbar();
				}
				this.panel.init();
				this.renderView('index');
			},
			fixDynNavbar : function() {
				/*
				 * FIX FOR JS ERROR
				 * "Uncaught TypeError: Cannot read property 'f7PageData' of undefined" in Navbar Remove Callback Function
				 * This is happens when <div class="navbar"> not added to page with dynamicNavbar option enabled
				 * When trying app.mainView.router.load F7 destroys events on old navbar
				 */
				var navbarHtml = '<div class="navbar"></div>';
				$$('.view').prepend(navbarHtml);
			}
		};

		app.mainView = F7.addView('.view-main', {
			dynamicNavbar : true,
			domCache : true
		});

		$$(document).on('click', '.backs', function(e) {
			app.mainView.router.back({
				pageName : 'index',
				force : true
			});
		});

		$$(document).click('.app-link', function(e) {
			e.preventDefault();
			var context = $$(this).data('context') ? JSON.parse($$(this).data('context')) : {};
			app.renderView($$(this).data('page'), context, $$(this).dataset());
			return false;
		});

		$$(document).click('.app-popup', function(e) {
			e.preventDefault();
			var name = $$(this).data('page');
			var context = (app.popup[name] && app.popup[name].context) ? app.popup[name].context : {};
			F7.popup(Template7.templates[ name ](context));
			return false;
		});

		// Export app to global
		window.F7 = F7;
		window.app = app;

	}(Framework7, Dom7, Template7, api));

var $$ = Dom7;

